﻿using System.Web.Mvc;
using BubbaGump.Models;
using BubbaGump.ViewModels;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace BubbaGump.Controllers
{
    public class ArticleController : RenderMvcController
    {
        public override ActionResult Index(ContentModel model)
        {
            Article article = model.Content as Article;

            var modifiedTitle = article.Title + " - Hard typed!";

            var viewModel = new ArticleViewModel(CurrentPage)
            {
                CustomTitle = modifiedTitle
            };

            return base.Index(viewModel);
        }
    }
}