﻿using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Models;

namespace BubbaGump.ViewModels
{
    public class ArticleViewModel : ContentModel
    {
        public string CustomTitle { get; set; }

        public ArticleViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}